package com.devopssample.service;

import com.devopssample.controller.TaskHelper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import junit.framework.TestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "file:src/main/webapp/WEB-INF/dispatcher-servlet.xml" })
public class TestClass extends TestCase {

	@Test
	public void testGenerateRandomNumber() {
		int generatedNumber = TaskHelper.generateRandomNumber();
		assertTrue(generatedNumber < 10);
	}
	
	@Test
	public void testFailedGenerateRandomNumber() {
		int generatedNumber = TaskHelper.generateRandomNumber();
		//updating code to test auto build trigger
		assertFalse(generatedNumber > 10);
	}

	@Test
	public void testCompare() {
		assertTrue(TaskHelper.compare(8));
	}

}
